# Contributing to TradingBot

Thank you for your interest in contributing to TradingBot! In this document, you will find information on setting up your development environment, code linting using Ruff, and working with pre-commit hooks.

## Setting Up Your Development Environment

1. Clone the repository:

   ```
   git clone https://gitlab.com/IgorEvsin/tradingbot.git
   cd tradingbot
   ```

2. Create a virtual environment and activate it:

   ```
   python -m venv venv
   source venv/bin/activate  # For Linux and macOS
   venv\Scripts\activate  # For Windows
   ```

3. Install the dependencies:

   ```
   pip install -r requirements.txt
   ```

## Code Linting (Ruff)

We use the Ruff linter (version 0.3.5) to check Python code. This helps ensure that the code meets quality standards and follows a consistent coding style.

### Using Ruff

1. Ensure that all dependencies from `requirements.txt` have been installed.

2. Run Ruff according to the configuration in the `pyproject.toml` file:

   ```
   ruff run
   ```

3. Ruff will check files according to your stylistic configuration and display warnings or errors, indicating the location of the issue and the type of rule violated.

4. Fix any issues found before submitting your code for review or committing.

### Pre-commit Hook Configuration

1. Ensure you have pre-commit installed:

   ```
   pip install pre-commit
   ```

2. Set up pre-commit hooks using the `.pre-commit-config.yaml` file:

   ```
   pre-commit install
   ```

3. Now, Ruff will automatically run to check your code when you commit. If issues are detected, they will be displayed, and your commit will be rejected.

4. If necessary, you can manually run the pre-commit hooks without committing:

   ```
   pre-commit run --all-files
   ```

## Additional Contribution Guidelines
