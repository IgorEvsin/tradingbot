def hello(name: str = "World") -> None:
    print(f"Hello, {name}!")


if __name__ == "__main__":
    hello()
