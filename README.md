# TradingBot

TradingBot is an automated solution designed for trading on financial markets using Tinkoff Invest API. The project aims to integrate with modern machine-learning (ML) models to generate signals for entering and exiting positions based on data from exogenous variables and market indicators. TradingBot provides a simple and convenient way to automate investment strategies and monitor portfolio development in real time.

## TradingBot main features:

1. **Integration with Tinkoff Invest API**: TradingBot securely connects to the Tinkoff Invest API to receive market data information, manipulate the portfolio and execute trading operations.

2. **Exogenous ML Models**: TradingBot provides the ability to integrate one or more machine-learned models to generate buy and sell signals based on exogenous data (such as economic indicators, news, social data and other variables).

3. **Expandability and flexibility**: TradingBot is able to work with a variety of trading algorithms and machine learning libraries, allowing users to customize the system to meet their individual needs.

4. **Realization of trading strategies**: TradingBot provides the ability to create and implement trading strategies by applying the signals obtained from ML models.

5. **Portfolio Monitoring and Order Execution**: TradingBot provides the use of tools for port monitoring


## Project Methodology

In TradingBot, we follow a structured methodology to ensure the consistent growth and maintenance of the project. Below are the key aspects of our project methodology:

### Branching Strategy

- `main` branch: The main branch remains stable and contains the latest production-ready version of the project.
- `develop` branch: This is the primary development branch, where all new features and improvements are merged. Once the `develop` branch is stable and ready for a release, it is merged into the `main` branch.
- Feature branches: For each new feature or bug fix, create a separate branch, branching off from the `develop` branch. Name the branches using the following pattern: `feature/description` (e.g., `feature/login-system`) or `bugfix/description` (e.g., `bugfix/fix-login-error`). Once the work is complete, submit a pull request to merge it back into the `develop` branch.

### Commit Message Guidelines

- Write descriptive and concise commit messages that clearly state the changes made in that commit.
- Use the imperative mood ("Add feature" instead of "Added feature").
- Start the commit message with an uppercase letter.
- If the commit relates to a specific issue or feature branch, include the issue number or feature name in the commit message.

### Code Review Process

- All code changes must go through a code review process before they are merged into the `develop` or `main` branch. This ensures that the code remains high quality and adheres to the project's guidelines.
- Create a pull request on GitHub to initiate the code review process.
- Assign at least one team member to review your pull request.
- Address any comments, suggestions, or requested changes from the reviewer.
- Once your changes are approved, the reviewer will merge your pull request into the target branch.

### Following Linting and Code Style Rules

- Follow the guidelines provided in the `CONTRIBUTING.md` file for the usage of linters and code style rules specific to this project.

## Getting Started

TBU

## Contributing

Please read our [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

TBU

## Acknowledgments

TBU
